package com.fjlopezs.openmrs;
import java.util.Date;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class OpenMRS
{
  private String user;
  private String pass;
  private String url;

  public OpenMRS(String user, String pass, String url)
  {
    this.user = user;
    this.pass = pass;
    this.url = url;
  }

  public Response agregarPersona(String givenName, String familyName, String gender)
    throws Exception
  {
    String input = "{\"gender\": \"" + gender + 
      "\", \"names\": [{\"givenName\":\"" + givenName + 
      "\", \"familyName\":\"" + familyName + "\"}]}";

    Response r = SimpleRESTful.getResponsePost(this.user, this.pass, this.url + "person", input);
    return r;
  }

  public Response agregarPaciente(String uuidPerson, int identifier)
    throws Exception
  {
    String output = SimpleRESTful.getResponseGet(this.user, this.pass, 
      this.url + "patientidentifiertype").getBody();

    Object obj = JSONValue.parse(output);
    JSONObject json = (JSONObject)obj;

    JSONArray jsonArray = (JSONArray)json.get("results");
    JSONObject rec = (JSONObject)jsonArray.get(0);

    String uuidPatientidentifiertype = (String)rec.get("uuid");

    output = SimpleRESTful.getResponseGet(this.user, this.pass, this.url + "location")
      .getBody();

    obj = JSONValue.parse(output);
    json = (JSONObject)obj;

    jsonArray = (JSONArray)json.get("results");
    rec = (JSONObject)jsonArray.get(0);
    String uuidLocationUnknow = (String)rec.get("uuid");

    String input = "{\"person\": \"" + uuidPerson + 
      "\", \"identifiers\": [{\"identifier\":\"" + identifier + 
      "\", \"identifierType\":\"" + uuidPatientidentifiertype + 
      "\", \"location\":\"" + uuidLocationUnknow + 
      "\", \"preferred\":true}]}";

    Response response = SimpleRESTful.getResponsePost(this.user, this.pass, this.url + "patient", input);

    return response;
  }

  public Response agregarPaciente(String givenName, String familyName, String gender, int identifier)
    throws Exception
  {
    Response r = agregarPersona(givenName, familyName, gender);

    Object obj = JSONValue.parse(r.getBody());
    JSONObject json = (JSONObject)obj;
    String uuidPerson = (String)json.get("uuid");

    String output = SimpleRESTful.getResponseGet(this.user, this.pass, 
      this.url + "patientidentifiertype").getBody();
    System.out.println(output);
    obj = JSONValue.parse(output);
    json = (JSONObject)obj;

    JSONArray jsonArray = (JSONArray)json.get("results");

    JSONObject rec = (JSONObject)jsonArray.get(0);

    String uuidPatientidentifiertype = (String)rec.get("uuid");

    output = SimpleRESTful.getResponseGet(this.user, this.pass, this.url + "location")
      .getBody();

    obj = JSONValue.parse(output);
    json = (JSONObject)obj;

    jsonArray = (JSONArray)json.get("results");
    rec = (JSONObject)jsonArray.get(0);
    String uuidLocationUnknow = (String)rec.get("uuid");

    String input = "{\"person\": \"" + uuidPerson + 
      "\", \"identifiers\": [{\"identifier\":\"" + identifier + 
      "\", \"identifierType\":\"" + uuidPatientidentifiertype + 
      "\", \"location\":\"" + uuidLocationUnknow + 
      "\", \"preferred\":true}]}";

    Response response = SimpleRESTful.getResponsePost(this.user, this.pass, this.url + "patient", input);

    return response;
  }

  public Response agregarPrestacion(Date dateEncounter, String uuidPerson, String uuidEncounterType)
    throws Exception
  {
    String date = new DateTime(dateEncounter).toString();
    String json = "{\"encounterDatetime\": \"" + 
      date + "\", " + 
      "\"patient\" : \"" + uuidPerson + "\", " + 
      "\"encounterType\": \"" + uuidEncounterType + "\"" + 
      "}";
    Response response = SimpleRESTful.getResponsePost(this.user, this.pass, this.url + "encounter", json);
    return response;
  }

  public Response agregarPrestacion(String uuidPerson, String uuidEncounterType) throws Exception {
    String date = new DateTime().toString();
    String json = "{\"encounterDatetime\": \"" + 
      date + "\", " + 
      "\"patient\" : \"" + uuidPerson + "\", " + 
      "\"encounterType\": \"" + uuidEncounterType + "\"" + 
      "}";
    Response response = SimpleRESTful.getResponsePost(this.user, this.pass, this.url + "encounter", json);
    return response;
  }

  public String buscarPaciente(String nombre)
    throws Exception
  {
    Response response = SimpleRESTful.getResponseGet(this.user, this.pass, this.url + "patient/?q=" + nombre);

    String var = new String();
    Object obj = JSONValue.parse(response.getBody());
    JSONObject json = (JSONObject)obj;

    JSONArray jsonArray = (JSONArray)json.get("results");

    @SuppressWarnings("unchecked")
	Iterator<JSONObject> recArr = jsonArray.iterator();
    while (recArr.hasNext()) {
      JSONObject rec = (JSONObject)recArr.next();
      var = var + ((String)rec.get("uuid")) + ";" + ((String)rec.get("display")) + "\n";
    }
    return var;
  }

  public String listarEncounterType()
    throws Exception
  {
    Response response = SimpleRESTful.getResponseGet(this.user, this.pass, this.url + "encountertype");

    String var = new String();
    Object obj = JSONValue.parse(response.getBody());
    JSONObject json = (JSONObject)obj;

    JSONArray jsonArray = (JSONArray)json.get("results");

    @SuppressWarnings("unchecked")
	Iterator<JSONObject> recArr = jsonArray.iterator();
    while (recArr.hasNext()) {
      JSONObject rec = (JSONObject)recArr.next();
      var = var + ((String)rec.get("uuid")) + ";" + ((String)rec.get("display")) + "\n";
    }
    return var;
  }

  public String mostrarPrestacionesClinicas(String uuidPaciente)
    throws Exception
  {
    Response response = SimpleRESTful.getResponseGet(this.user, this.pass, this.url + "encounter?patient=" + uuidPaciente);

    String var = new String();
    Object obj = JSONValue.parse(response.getBody());
    System.out.println(response.getBody());
    JSONObject json = (JSONObject)obj;

    JSONArray jsonArray = (JSONArray)json.get("results");

    @SuppressWarnings("unchecked")
	Iterator<JSONObject> recArr = jsonArray.iterator();
    while (recArr.hasNext()) {
      JSONObject rec = (JSONObject)recArr.next();
      var = var + "rec " + ((String)rec.get("uuid")) + 
        ";" + ((String)rec.get("display")) + 
        ";" + ((String)rec.get("encounterDatetime")) + 
        ";" + ((String)rec.get("location")) + 
        ";" + ((String)rec.get("provider")) + 
        "\n";
    }
    return var;
  }
}