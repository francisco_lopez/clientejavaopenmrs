package com.fjlopezs.openmrs;
/**
 * 
 * @author Francisco L�pez <f.lopez02@ufromail.cl>
 *
 */
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;

public class SimpleRESTful
{
  public static Response getResponsePost(String username, String password, String url, String json)
    throws AuthenticationException, ClientProtocolException, IOException
  {
    DefaultHttpClient httpclient = new DefaultHttpClient();
    try {
      HttpPost httpPost = new HttpPost(url);
      UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
      BasicScheme scheme = new BasicScheme();
      Header authorizationHeader = scheme.authenticate(credentials, httpPost);
      httpPost.setHeader(authorizationHeader);

      StringEntity input = new StringEntity(json);
      input.setContentType("application/json");
      httpPost.setEntity(input);

      HttpResponse responseRequest = httpclient.execute(httpPost);
      int rc = responseRequest.getStatusLine().getStatusCode();
      if ((rc != 204) && (rc != 201) && (rc != 200)) {
        throw new RuntimeException("Failed : HTTP error code : " + responseRequest.getStatusLine().getStatusCode());
      }
      return new Response(responseRequest);
    } finally {
      httpclient.getConnectionManager().shutdown();
    }
  }

  public static Response getResponseGet(String username, String password, String url)
    throws AuthenticationException, ClientProtocolException, IOException
  {
    DefaultHttpClient httpclient = new DefaultHttpClient();
    try {
      HttpGet httpGet = new HttpGet(url);

      UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
      BasicScheme scheme = new BasicScheme();
      Header authorizationHeader = scheme.authenticate(credentials, httpGet);
      httpGet.setHeader(authorizationHeader);
      HttpResponse responseRequest = httpclient.execute(httpGet);
      int rc = responseRequest.getStatusLine().getStatusCode();
      if ((rc != 204) && (rc != 201) && (rc != 200)) {
        throw new RuntimeException("Failed : HTTP error code : " + responseRequest.getStatusLine().getStatusCode());
      }
      return new Response(responseRequest);
    } finally {
      httpclient.getConnectionManager().shutdown();
    }
  }
}