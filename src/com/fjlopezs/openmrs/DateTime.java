package com.fjlopezs.openmrs;
/**
 * 
 * @author Francisco L�pez <f.lopez02@ufromail.cl>
 *
 */
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTime
{
  private String dateTime;

  public DateTime(Date fecha)
  {
    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    this.dateTime = formatter.format(fecha).toString();
  }

  public DateTime() {
    Calendar calendario = GregorianCalendar.getInstance();
    Date fecha = calendario.getTime();
    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    this.dateTime = formatter.format(fecha).toString();
  }

  public String getDateTime()
  {
    return this.dateTime; }

  public String toString() {
    return this.dateTime;
  }
}