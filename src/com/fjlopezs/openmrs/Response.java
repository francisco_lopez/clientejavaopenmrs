package com.fjlopezs.openmrs;
/**
 * 
 * @author Francisco L�pez <f.lopez02@ufromail.cl>
 *
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;
import org.apache.http.HttpResponse;

public class Response
{
  private String header;
  private String body;
  private int code;

  public int getCode()
  {
    return this.code;
  }

  public Response(HttpResponse response) throws IllegalStateException, IOException {
    this.header = response.toString();
    this.body = convertStreamToString(response.getEntity().getContent());
    this.code = response.getStatusLine().getStatusCode();
  }

  public String getHeader()
  {
    return this.header;
  }

  public String getBody()
  {
    return this.body; }

  private static String convertStreamToString(InputStream is) {
    try {
      return new Scanner(is).useDelimiter("\\A").next(); } catch (NoSuchElementException e) {
    }
    return "";
  }
}